import React from 'react';
import Header from './Header';
import MainContent from './MainContent';
import Navigation from './Navigation';

const Layout = ({children}) => {
    return (
        <div>
            <Navigation/>
            {/* <Header/> */}
            <MainContent>
                {children}
            </MainContent>
        </div>
    );
}

export default Layout;
