import React from "react";
import { Link } from "react-router-dom";

const Navigation = () => {
    return (
        <nav class="pcoded-navbar">
            <div class="navbar-wrapper">
                <div class="navbar-brand header-logo">
                    <Link to="/" class="b-brand">
                        <div class="b-bg">
                            <i class="feather icon-trending-up"></i>
                        </div>
                        <span class="b-title">LOGO - CITE</span>
                    </Link>
                    <a
                        class="mobile-menu"
                        id="mobile-collapse"
                        href="javascript:"
                    >
                        <span></span>
                    </a>
                </div>
                <div class="navbar-content scroll-div">
                    <ul class="nav pcoded-inner-navbar">
                        <li class="nav-item pcoded-menu-caption">
                            <span class="pcoded-micon">
                                <i class="feather icon-users"></i>
                            </span>
                            <span class="pcoded-mtext"> Organización</span>
                        </li>

                        <li
                            data-username="form elements advance componant validation masking wizard picker select"
                            class="nav-item"
                        >
                            <Link class="nav-link " to="/materia">
                                <span class="pcoded-micon">
                                    <i class="feather icon-file-text"></i>
                                </span>
                                <span class="pcoded-mtext">Materias</span>
                            </Link>
                        </li>
                        <li
                            data-username="form elements advance componant validation masking wizard picker select"
                            class="nav-item"
                        >
                            <Link class="nav-link " to="/students">
                                <span class="pcoded-micon">
                                    <i class="feather icon-file-text"></i>
                                </span>
                                <span class="pcoded-mtext">Estudiantes</span>
                            </Link>
                        </li>

                        <li class="nav-item pcoded-menu-caption">
                            <span class="pcoded-micon">
                                <i class="feather icon-codepen"></i>
                            </span>
                            <span class="pcoded-mtext"> Cursos</span>
                        </li>
                        <li
                            data-username="form elements advance componant validation masking wizard picker select"
                            class="nav-item"
                        >
                            <Link class="nav-link " to="/courses">
                                <span class="pcoded-micon">
                                    <i class="feather icon-file-text"></i>
                                </span>
                                <span class="pcoded-mtext">Cursos</span>
                            </Link>
                        </li>

                        <li class="nav-item pcoded-menu-caption">
                            <span class="pcoded-micon">
                                <i class="feather icon-clipboard"></i>
                            </span>
                            <span class="pcoded-mtext"> Certificados</span>
                        </li>
                        <li data-username="Charts Morris" class="nav-item">
                            <Link
                                to="/certificado/individual"
                                class="nav-link "
                            >
                                <span class="pcoded-micon">
                                    <i class="feather icon-file"></i>
                                </span>
                                <span class="pcoded-mtext">Individual</span>
                            </Link>
                        </li>
                        <li data-username="Maps Google" class="nav-item">
                            <Link to="/certificado/grupal" class="nav-link ">
                                <span class="pcoded-micon">
                                    <i class="feather icon-copy"></i>
                                </span>
                                <span class="pcoded-mtext">Grupales</span>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default Navigation;
