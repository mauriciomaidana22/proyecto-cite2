import React from 'react';
import ReactDOM from 'react-dom';
import App from './routes/App';
/* import ScriptTag from 'react-script-tag' */

import "./assets/css/style.css"
import "./assets/fonts/fontawesome/css/fontawesome-all.min.css"
import "./assets/plugins/animation/css/animate.min.css"


ReactDOM.render(
  <React.StrictMode>
    <App />
{/*     <ScriptTag src="assets/js/vendor-all.min.js"/>
    <ScriptTag src="assets/js/pcoded.min.js"/>
    <ScriptTag src="assets/plugins/bootstrap/js/bootstrap.min.js"/> */}
  </React.StrictMode>,
  document.getElementById('root')
);